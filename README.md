## About

**TDWE**: The Devs Windows Environment

<img src="https://i.imgur.com/N8oBUE7.png">

So you have a fresh install of Windows 11: Let's configure most things in just 1 script!
* Created by Dylan Hunt <dylan@imperium42.com>, combining web contributions and my own.
* Feel free to PR contributions - I may have missed something important!

## By default, what does this PS module do?
Tested on Win11 via a fresh install:

1. Backup-CoreRegistry
1. Install-Wsl2Ubuntu # Install before everything else! Fails if pending reboot.
1. Install-PkgMgrs
1. Install-Git
1. Set-WinDevSettings
1. Install-DevTools
1. Install-HomeApps
1. Install-ConfigWinUpdates
1. Optimize-WindowsApps

**This also adds:**

1. `elevate` cmd to $PROFILE | Elevates to admin in same working dir

## Prerequisites
1. Must allow PS scripts: In PS Admin, run: `Set-ExecutionPolicy -ExecutionPolicy unrestricted`. You will still get warnings before a script runs. 

## Setup
1. Inspect the files and comment out the apps/settings you don't want.
1. Copy `/src/config.template.yaml` to `/src/config.yaml` and fill
1. Run `/src/TDWE.ps1` -> reboot when done
1. Run `TDWE-PostReboot.ps1` for final cleanup

## TODO
- ~~Better organization - split file into modules.~~
- ~~More comments.~~
- ~~More echos.~~
- Reminders for what we cannot automatically do.
    - For what we can't do, perhaps wget -> launch the installer?

## LICENSE
MIT
