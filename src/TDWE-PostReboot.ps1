#######################################################
# After you reboot, run this to finish up / wrap up
#######################################################
# TODO: Port redundant vars over from the other script
$Wsl2LinuxDistro = "Ubuntu"
#######################################################
echo ""
echo "-----------------------------------------------------"
echo "[TDWE] Running post-install-reboot cleanup scripts..."
echo "."

Set-Wsl2DistroDefault # Sometimes Docker's can set default, blocking bash from opening
Remove-ExplorerCache
Verify-Integrity

###############################################################################
# Sometimes Docker's can set default, blocking bash from opening
Set-Wsl2DistroDefault
{
    param([string]$Wsl2LinuxDistro)
    
    echo "Setting '$Wsl2LinuxDistro' as default wsl2 distro ..."
    wsl --set-default $Wsl2LinuxDistro
    wsl -l -v # Just lists them to sanity check
}

function Remove-ExplorerCache
{
    # Explorer
    echo ""
    echo "Clearing Explorer cache ..."

    Stop-Process -Name explorer -Force
    Start-Process explorer
}

# We *possibly* edited registry entries: Just a sanity check
function Verify-Integrity
{
    echo ""
    echo "Since we changed registry vals earlier: Verifying core OS file integrity with SFC + DISM scans ..."

    sfc /scannow
    DISM /Online /Cleanup-Image /RestoreHealth
}
