#################################################
# pwsh v7+ allows running multiple tasks async
#################################################
# (!) Path acts as if I'm still at the root of src/
$ConfigPathToYaml = ".\config.yaml"
$PathToManifest = ".\private\TheDevsWinEnv-Manifest.psd1";
#################################################

# Relative path to "TheDevsWinEnv-Manifest.psd1"
function Import-AllModules
{
    echo ""
    echo "[TDWE-AfterPwsh7] Importing manifest of modules (via psd1 manifest) ..."
    ipmo -Name $PathToManifest -Force
}

# -----------------------------------------------------------------------------------
# Init >>
echo "###########################################"
echo "(!) Continuing TDWE via ./private/TDWE-AfterPwsh7.ps1 | parallel support here for simultaneous installs!"
echo "###########################################"
echo ""
pause

Import-AllModules
Import-Config -ConfigPathToYaml $ConfigPathToYaml

# -----------------------------------------------------------------------------------
# Main >>
Write-Host "[TDWE-AfterPwsh7] Finished initializing: Proceed with Set-WinDevSettings? " -NoNewline; pause

Set-WinDevSettings `
    -DisableShowMoreOptsRightClick $DisableShowMoreOptsRightClick
if ($PauseBetweenSections) { Write-Host "[TDWE-AfterPwsh7] Finished Set-WinDevSettings (Next: Install-DevTools). " -NoNewline; pause }

Install-DevTools `
    -WorkEmail $WorkEmail
if ($PauseBetweenSections) { Write-Host "[TDWE-AfterPwsh7] Finished Install-DevTools (Next: Install-HomeApps). " -NoNewline; pause }

Install-HomeApps
if ($PauseBetweenSections) { Write-Host "[TDWE-AfterPwsh7] Finished Install-HomeApps (Next: Optimize-WindowsApps). " -NoNewline; pause }

Optimize-WindowsApps `
    -IsPrivateNetwork $IsPrivateNetwork `
    -PreferredDns $PreferredDns `
    -PreferredDnsAlt $PreferredDnsAlt
if ($PauseBetweenSections) { Write-Host "[TDWE-AfterPwsh7] Finished Optimize-WindowsApps (Next: Disable-Telemetry). " -NoNewline; pause }

Install-ConfigWinUpdates
if ($PauseBetweenSections) { Write-Host "[TDWE-AfterPwsh7] Finished Install-ConfigWinUpdates (Next: Done). " -NoNewline; pause }

# -----------------------------------------------------------------------------------
# DONE >> After reboot, run TDWE-PostReboot.ps1
echo ""
echo "---------------------------"
echo "[TDWE-AfterPwsh7] Done - REBOOT ASAP before opening anything => then run TDWE-PostReboot.ps1"
echo -NoNewLine 'Press any key to exit...';
$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');
Exit
