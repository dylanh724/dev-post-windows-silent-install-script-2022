function Elevate {
    Start-Process wt -ArgumentList '-d', (Get-Location) -Verb RunAs
}

function which($name) {
    Get-Command $name | Select-Object -ExpandProperty Source
}

function touch {
    param(
        [Parameter(Mandatory=$true)] [string] $path
    )
    if (Test-Path $path) {
        (Get-Item -Path $path).LastWriteTime = Get-Date
    } else {
        New-Item -ItemType File -Path $path | Out-Null
    }
}