################################################################################################################
#
# private/TheDevsWinEnv.psm1
# Created by Dylan Hunt <dylan@imperium42.com>
# TODO: Split this into multiple modules via psd1.NestedModules
#
################################################################################################################

# ----------------------------------------------------------------------
#region Utils

function Restart-Explorer 
{
    Get-Process explorer | Stop-Process -Force
    Start-Process explorer
    Write-Host "Explorer has been restarted."
}

function Import-Config 
{
    param(
        [string]$ConfigPathToYaml
    )

    try {
        # Check if the powershell-yaml module is installed
        if (-not (Get-Module -ListAvailable -Name powershell-yaml)) {
            # Install the powershell-yaml module, if not yet installed
            Install-Module -Name powershell-yaml -Scope CurrentUser -Force -AllowClobber
        }

        # Import the powershell-yaml module
        Import-Module powershell-yaml

        # Load the YAML configuration
        $Config = ConvertFrom-Yaml (Get-Content $ConfigPathToYaml -Raw)

        # Convert boolean values and expand environment variables in paths
        $IsPrivateNetwork = [bool]$Config.IsPrivateNetwork
        $DisableShowMoreOptsRightClick = [bool]$Config.DisableShowMoreOptsRightClick
        $PauseBetweenSections = [bool]$Config.PauseBetweenSections

        # Access other variables
        $FullName = $Config.FullName
        $WorkEmail = $Config.WorkEmail
        $GitRootDir = $Config.GitRootDir
        $PreferredDns = $Config.PreferredDns
        $PreferredDnsAlt = $Config.PreferredDnsAlt
        $Wsl2LinuxDistro = $Config.Wsl2LinuxDistro

        echo ""
        echo "##############################"
        echo "Loaded config for '$FullName'"
        echo "Work Email: $WorkEmail"
        echo "Git Root Directory is set to: $GitRootDir"
        echo "Is Private Network: $IsPrivateNetwork"
        echo "Preferred DNS: $PreferredDns"
        echo "Preferred DNS Alt: $PreferredDnsAlt"
        echo "WSL 2 Linux Distro: $Wsl2LinuxDistro"
        echo "Disable 'Show More Options' in Right Click: $DisableShowMoreOptsRightClick"
        echo "Pause Between Sections: $PauseBetweenSections"
        echo "##############################"
    }
    catch {
        Write-Error "Error occurred: $_"
        throw $_ # Stop the script, re-throwing the caught exception
    }
    
    echo ""
    pause
}

# Only after choco is installed
function RefreshTerminal
{
    Import-Module $env:ChocolateyInstall\helpers\chocolateyProfile.psm1
    refreshenv
}

# Example usage:
# (1) Install-AppsViaChoco -ChocoApps @("git", @{ name = "nodejs"; params = "/InstallDir:C:\nodejs" })
# (2) Install-AppsViaChoco -ChocoApps @("git", "nodejs")
# (3) Install-AppsViaChoco -ChocoApps "git"
# (4) Install-AppsViaChoco -ChocoApps @{ name = "nodejs"; params = "/InstallDir:C:\nodejs" }
function Install-AppsViaChoco {
    param (
        [Parameter(Mandatory=$true)]
        [System.Object]$ChocoApps
    )

    # Ensure input is treated as an array, even if it's a single string or object
    if (-not $ChocoApps -is [System.Array]) 
    {
        $ChocoApps = @($ChocoApps)
    }

    foreach ($App in $ChocoApps) 
    {
        # Determine if input is a string or a hashtable and prepare variables
        if ($App -is [string]) 
        {
            $appName = $App
            $appParams = $null
        } 
        elseif ($App -is [System.Collections.Hashtable])
        {
            $appName = $App['name']
            $appParams = $App['params']
        } 
        else 
        {
            Write-Output "Unsupported input type: $App"
            continue
        }

        # Construct the command string
        $command = "choco install $appName"
        if ($appParams) {
            $command += " --params `"$appParams`""
        }
        $command += " --yes"

        Write-Output ""
        Write-Output "---------------------------"
        Write-Output "[Install-AppsViaChoco] Installing $appName (via choco) with params: $appParams"

        # Execute the command
        Invoke-Expression $command
    }
}

# Example usage:
# (1) Install-AppsViaChoco -ChocoApps @("git", @{ name = "nodejs"; params = "/InstallDir:C:\nodejs" })
# (2) Install-AppsViaChoco -ChocoApps @("git", "nodejs")
# (3) Install-AppsViaChoco -ChocoApps "git"
# (4) Install-AppsViaChoco -ChocoApps @{ name = "nodejs"; params = "/InstallDir:C:\nodejs" }
# (!) If array of > 1 and running pwsh, we will install in parallel!
function Install-AppsViaChoco {
    param (
        [Parameter(Mandatory = $true)]
        [System.Object]$ChocoApps
    )

    # Ensure input is treated as an array
    if (-not $ChocoApps -is [System.Array]) {
        $ChocoApps = @($ChocoApps)
    }

    # Determine if PowerShell 7+ is used and multiple apps are provided
    if ($PSVersionTable.PSVersion.Major -ge 7 -and $ChocoApps.Count -gt 1) {
        # Parallel execution
        $ChocoApps | ForEach-Object -Parallel {
            $command = "choco install $($_.name)"
            if ($_.params) {
                $command += " --params `"$($_.params)`""
            }
            $command += " --yes"
            Invoke-Expression $command
        }
    } else {
        # Sequential execution
        foreach ($App in $ChocoApps) {
            # Determine if input is a string or a hashtable and prepare variables
            if ($App -is [string]) {
                $appName = $App
                $appParams = $null
            } elseif ($App -is [System.Collections.Hashtable]) {
                $appName = $App['name']
                $appParams = $App['params']
            } else {
                Write-Output "Unsupported input type: $App"
                continue
            }

            # Construct the command string
            $command = "choco install $appName"
            if ($appParams) {
                $command += " --params `"$appParams`""
            }
            $command += " --yes"

            Write-Output ""
            Write-Output "---------------------------"
            Write-Output "[Install-AppsViaChoco] Installing $appName (via choco) with params: $appParams"

            # Execute the command
            Invoke-Expression $command
        }
    }
}


function Assert-IsNvidiaGpu
{
    $gpu = (Get-WmiObject Win32_VideoController).Name.toLowerInvariant()
    $IsNvidia = $gpu -like "*nvidia*"

    return $IsNvidia
}

# Most installations require admin
function Assert-RunAsAdmin
{
    echo ""
    echo "Ensuring running as admin ..."

    # Ask for elevated permissions if required
    # From https://github.com/n1snt/Windows-Decrapifier/blob/main/decrapify.ps1
    If (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole]"Administrator")) {
        Start-Process powershell.exe "-NoProfile -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs
        Exit
    }
}

# Backs up HKLM, HKCU, HKCR to c:\reg-bak-from-ps1
function Backup-CoreRegistry
{
    echo ""
    echo "[Backup-CoreRegistry] Backing up registry (HKLM, HKCU, HKCR) to 'c:\reg-bak-from-ps1\' ..."

    $regBakDir = 'c:\reg-bak-from-ps1'
    mkdir $regBakDir -f

    # If you run this multiple times, we don't want to overwrite your vanilla bak, for example!
    $timestamp = Get-Date -Format o | ForEach-Object { $_ -replace ":", "." }
    mkdir $regBakDir\$timestamp -f

    { reg export HKLM C:\registry-backup-hklm.reg | Out-Null }
    { reg export HKCU C:\registry-backup-hkcu.reg | Out-Null }
    { reg export HKCR C:\registry-backup-hkcr.reg | Out-Null }

    echo "[Backup-CoreRegistry] Done backing up!"
    echo ""
}

function Force-WinExplorerRestart
{
    Echo "Restarting Explorer ..."
    Stop-Process -processName: Explorer -force
}
#endregion /Utils


# ----------------------------------------------------------------------
#region Installs

# Install this BEFORE everything else (pending reboot blocks this).
# (!) TODO: Docker Desktop may set itself default later.
#           Perhaps this should come *after* core dev apps?
#           Or an --arg to prevent setting default on choco Docker-Desktop install?
function Install-Wsl2Ubuntu
{
    param([string]$Wsl2LinuxDistro)

    echo ""
    echo "[Install-Wsl2Ubuntu] Enabling WSL2 feature ..."

    # It's an optional feat, so we first enable it.
    dism.exe /online /enable-feature /featurename:Microsoft-Windows-Subsystem-Linux /all /norestart

    # Install WSL2 + Ubuntu.
    echo ""
    echo "[Install-Wsl2Ubuntu] Installing wsl2 distro: '$Wsl2LinuxDistro' ..."
    wsl --install -d $Wsl2LinuxDistro
    
    echo ""
    echo "Once you reboot, you'll be able to see '$Wsl2LinuxDistro' in the wsl2 list + set default."
    echo ""
}

function Install-ConfigWinUpdates
{
    # Restrict Windows Update P2P only to local network
    # Disable Windows Update automatic restart
    function Disable-WinP2pOptimizationUpdateDeliveryOpts
    {
        echo ""
        echo "[Install-ConfigWinUpdates] Disabling Windows Update automatic restart ..."
        Set-ItemProperty -Path "HKLM:\Software\Microsoft\WindowsUpdate\UX\Settings" `
            -Name "UxOption" -Type DWord -Value 1

        # Update & Security -> Windows Update -> Advanced -> Choose how updates are delivered -> Updates from more than one place (this is a GUI bug, registry is set properly even though it may show 'ON')
        New-ItemProperty -ErrorAction SilentlyContinue -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\DeliveryOptimization\Config" `
            -Name "DownloadMode" -PropertyType DWORD -Value 0 | Out-Null

        Set-ItemProperty -ErrorAction SilentlyContinue -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\DeliveryOptimization\Config" `
            -Name "DODownloadMode" -Value 0 | Out-Null

        Set-ItemProperty -ErrorAction SilentlyContinue -Path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\DeliveryOptimization\" `
            -Name "SystemSettingsDownloadMode" -Value 0 | Out-Null
    }

    # Install Windows Updates >> https://pureinfotech.com/install-windows-10-update-powershell/
    function Update-Windows
    {
        Install-Module PSWindowsUpdate -Force
        Get-WindowsUpdate
        Install-WindowsUpdate -AcceptAll -ForceDownload -ForceInstall
    }

    # Install Windows Store updates (eg: `Notepad`, to allow dark theme).
    function Update-WinStoreApps
    {
        echo ""
        echo "[Install-ConfigWinUpdates] Updating Windows Store apps ..."

        $namespaceName = "root\cimv2\mdm\dmmap"
        $className = "MDM_EnterpriseModernAppManagement_AppManagement01"

        try {
            # Using Get-CimInstance to retrieve the WMI object
            $cimObj = Get-CimInstance -Namespace $namespaceName -ClassName $className

            # Invoking the UpdateScanMethod using Invoke-CimMethod
            $result = Invoke-CimMethod -InputObject $cimObj -MethodName "UpdateScanMethod"

            # Output result or success message
            Write-Output "Update initiated successfully. Result: $result"
        } catch {
            Write-Error "Failed to invoke update method: $_"
        }
    }

    echo ""
    echo "[Install-ConfigWinUpdates] Configuring Windows Updates, then updating ..."
    Disable-WinP2pOptimizationUpdateDeliveryOpts
    Update-WinStoreApps
    Update-Windows # Will require a reboot

}

function Install-Pwsh7
{
    echo ""
    echo "[Install-Pwsh7] Installing PowerShell 7 (allows parallel processes) ..."

    # Install PowerShell 7
    $ChocoApp = 'powershell-core'
    Install-AppsViaChoco -ChocoApps $ChocoApp
    RefreshTerminal
}

# Just the core ones like Choco to get started. pip/condo/npm etc are in the dev install scripts.
function Install-CorePkgMgrs
{
    function Install-ChocoPkgMgr
    {
        echo ""
        echo "Installing Choco pkg mgr ..."

        Set-ExecutionPolicy Bypass -Scope Process -Force; [System.Net.ServicePointManager]::SecurityProtocol = `
            [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; `
            iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))

    	RefreshTerminal
    }

    function Install-NugetPkgMgr
    {
        echo ""
        echo "Installing Nuget pkg mgr ..."
        Install-PackageProvider -Name NuGet -Force
    }

    echo ""
    echo "Installing core pkg mgrs >>"
    Install-ChocoPkgMgr
    Install-NugetPkgMgr
}

function Install-Git
{
    param(
        [string]$GitRootDir,
        [string]$FullName,
        [string]$WorkEmail
    )

    function Install-InitGit
    {
        echo ""
        echo "Creating git dirs @ $GitRootDir ..."

        # Create nested dir for root
        new-item -type directory -path $GitRootDir -Force

        echo ""
        echo "Installing Git ..."

        $ChocoApp = 'git --params "/GitAndUnixToolsOnPath"'
        Install-AppsViaChoco -ChocoApps $ChocoApp
        RefreshTerminal
    }

    function Set-GitCore
    {
        echo ""
        echo "Configuring Git core settings ..."

        git config --global core.editor "code --wait"
        git config --global init.defaultBranch main
        git config --global user.name $FullName
        git config --global user.email $WorkEmail
    }

    function Set-GitAliases
    {
        echo ""
        echo "Configuring Git useful aliases ..."

        git config --global alias.last 'log -1 HEAD'
        git config --global alias.ls "log --pretty=format:'%C(yellow)%h %ad%Cred%d %Creset%s%Cblue [%cn]' --decorate --date=short --graph"
        git config --global alias.standup "log --since yesterday --author $(git config user.email) --pretty=short"
        git config --global alias.ammend "commit -a --amend"
        git config --global alias.everything "! git pull && git submodule update --init --recursive"
        git config --global alias.aliases "config --get-regexp alias"
    }

    echo ""
    echo "Installing and configuring Git >>"
    Install-InitGit
    Set-GitCore
    Set-GitAliases
}

function Install-DevTools
{
    param([string]$WorkEmail)

    function Install-VS-Tools
    {
        echo ""
        echo "[Install-DevTools] Installing tools related to VS (Studio, Code, Runtimes) ..."

        function Install-ChocoDevTools
        {
            $ChocoVSTools = @(
                'vscode'
                'vcredist140'                               # Visual C++ redistributable for VS 2015~2022
                'visualstudio2022community'
                'visualstudio2022-workload-nativedesktop'
                'visualstudio2022-workload-manageddesktop'  # .NET Desktop Development workload for VS22
                'visualstudio2022-workload-nativegame'      # Game development with C++ workload for VS22
                #'visualstudio2022buildtools'                # Build tools for VS22; required for Cargo (Rust) + more C++ CLI ops | https://visualstudio.microsoft.com/visual-cpp-build-tools/ | TODO: Look into stability before using
                'dotnet4.6.1'                               # For projects made in VS2019, that are oddly !included with 2019. 4.8 is included in VS2022.
            )

            Install-AppsViaChoco -ChocoApps $ChocoVSTools
        }

        function Install-WingetDevTools
        {
            $WingetApps = @(
                @{ Name = 'WinDbg'; Source = 'msstore' }                        # Crash dump analyzer
                @{ Name = '7zip.7zip'; Source = 'winget' }                      # 7z archive support
                @{ Name = 'Microsoft.VisualStudioCode'; Source = 'winget' }     # VSCode
                @{ Name = 'Visual Studio Community 2022'; Source = 'msstore' }  # VS2022
                @{ Name = 'Rustlang.Rust.MSVC'; Source = 'winget' }             # Rustlang + Toolchain + Cargo
            )

            Install-AppsViaWinget -WingetApps $WingetApps
        }

        function Add-7zToPath 
        {
            echo ""
            echo "Adding 7z dir to path ..."
            
            # Specify the path to the directory containing the 7z executable.
            # This is typically "C:\Program Files\7-Zip" if installed in the default location.
            $7zDir = "C:\Program Files\7-Zip"

            if (-not (Test-Path $7zDir)) {
                Write-Host "The specified path to 7-Zip does not exist: $7zDir"
                return
            }

            # Get the current PATH environment variable for the current user or system.
            # Replace 'User' with 'Machine' if you want to set it system-wide (requires admin privileges).
            $path = [System.Environment]::GetEnvironmentVariable('PATH', [System.EnvironmentVariableTarget]::User)

            # Check if 7z is already in the PATH
            if ($path -notlike "*$7zDir*") {
                # If not, add the 7z directory to the PATH.
                $newPath = $path + ";" + $7zDir
                [System.Environment]::SetEnvironmentVariable('PATH', $newPath, [System.EnvironmentVariableTarget]::User)

                Write-Host "7-Zip directory has been added to PATH for the current user."
            } else {
                Write-Host "7-Zip directory is already in PATH."
            }
        }

        Install-ChocoDevTools
        Install-WingetDevTools
        Add-7zToPath
        RefreshTerminal
    }

    function Install-CoreDevTools
    {
    	function Install-ForkGitClient
    	{
    		# Define the URL to download the Fork installer
    		$ForkExe = "Fork-1.95.exe"
    		$installerUrl = "https://cdn.fork.dev/win/$ForkExe" # The ver !matters; it'll auto-update within

    		# Define the path where you want to save the installer
    		$installerPath = "$env:TEMP\$ForkExe"

    		# Download the installer
    	        echo "[Install-CoreDevTools] Downloading 'Fork Git Client' installer manually ..."
    		Invoke-WebRequest -Uri $installerUrl -OutFile $installerPath

    		# Attempt silent installation if supported
    		if (Test-Path $installerPath) {
    		    # TODO: --name and --email (sent feat request) silent args

    		    # Start the installer
    		    echo "[Install-CoreDevTools] Starting 'Fork Git Client' installer (!silent) ..."
    		    Start-Process -FilePath $installerPath
    		} else {
    		    Write-Host "Failed to download Fork installer."
    		}
    	}

        # TODO: Move to a less "core" func
        function Setup-OhMyPosh
    	{
            # oh-my-posh was installed earlier
            oh-my-posh font install FiraCode

            # Configure oh-my-posh to default run each session start
            $ohMyPoshInitCmd = "oh-my-posh --init --shell pwsh --config ~/AppData/Local/Programs/oh-my-posh/themes/jandedobbeleer.omp.json | Invoke-Expression"

            # Check if the PowerShell profile exists, create if not
            If (-not (Test-Path -Path $PROFILE)) {
                New-Item -ItemType File -Path $PROFILE -Force
            }

            # Check if Oh My Posh command is already in the profile
            $profileContent = Get-Content -Path $PROFILE
            If (-not ($profileContent -contains $ohMyPoshInitCmd)) {
                # Append the command to the profile
                Add-Content -Path $PROFILE -Value $ohMyPoshInitCmd
                Write-Output "Oh My Posh initialization command added to your PowerShell profile."
            } Else {
                Write-Output "Your PowerShell profile already contains the Oh My Posh initialization command."
            }
    	}

	    # Requires oh-my-posh
        # TODO: Move to a less "core" func
    	function Setup-PrettyTerminal
    	{
            Setup-OhMyPosh

            # Define the source and destination paths
            $sourcePath = ".\private\terminal-settings.json"
            $destinationPath = Join-Path -Path $env:USERPROFILE -ChildPath "AppData\Local\Packages\Microsoft.WindowsTerminal_8wekyb3d8bbwe\LocalState\settings.json"

            # Check if the source file exists
            If (Test-Path -Path $sourcePath) {
                # Optionally, create a backup of the existing destination file
                $backupPath = $destinationPath + "_backup_" + (Get-Date -Format "yyyyMMddHHmmss") + ".json"
                If (Test-Path -Path $destinationPath) {
                    Copy-Item -Path $destinationPath -Destination $backupPath
                    Write-Output "Backup of the existing settings.json created at: $backupPath"
                }

                # Copy the source file to the destination
                Copy-Item -Path $sourcePath -Destination $destinationPath -Force
                Write-Output "Terminal settings have been updated from: $sourcePath"
            } Else {
                Write-Output "Source file not found at: $sourcePath"
            }

            RefreshTerminal
    	}

        echo ""
        echo "[Install-DevTools] Installing core dev tools (7z, Slack, Python, etc)  ..."

        function Install-ChocoDevApps
        {
            # Git installation is in a different func.
            $ChocoApps = @(
                'nvm' # Node.js version manager for Windows (also includes npm+yarn)
                'notepadplusplus' # Speedy multi-tab IDE that doesn't require saving (like dev sticky notes)
                'jetbrainstoolbox' # Install Rider+Webstorm+Resharper thru here.
                #'anaconda3 --params "/AddToPath"' # Python pkg mgr similar to pip/npm. # BUG: Broken
                #'7zip' # 7z+tar+gz+cli archive support # BUG: Failing
                'mongodb-compass' # Mongodb NoSQL GUI
                #'git-fork' # BUG: Failing
                'docker-desktop' # (!) This sets default wsl2, rendering `bash` not working until you set 'Ubuntu' distro default again
                'kubernetes-cli' # Includes kubectl for clusters/pods/servers
                'kubernetes-helm' # Kubernetes helper
                'fiddler' # Decrypt/sniff https traffic: Useful for debugging HTTP calls (online multiplayer)
                'cmake' # Porting over some Unix make tools
                'make' # Porting over some Unix make tools
                'postman' # For HTTP+POST+GET type call (API/REST) testing/mocking
                'unity-hub' # Unity engine parent installer
                'awscli' # AWS CLI tools
                'openssh' # Useful net cli tools like `ssh-copy-id`
                #'tortoisesvn' # If you're oldschool and use SVN instead of Git
                'winscp' # Like Filezilla, but no more secure and with no spyware
                'azure-cli' # Azure CLI tools, similar to AWS CLI
                'gh' #GitHub CLI
                'ChocolateyGUI' # Offers a Windows Store-like experience
                'bind-toolsonly' # DNS CLI tools, like dig.exe (host name -> to IP)
                'pnpm' # The successor to npm+yarn, driving popularity fast
                'virtualbox' # Great cross-platform VM tool, +cli support
            )

            Install-AppsViaChoco -ChocoApps $ChocoApps
        }

        function Install-WingetDevApps
        {
            $WingetApps = @(
                @{ Name = 'Python 3.10'; Source = 'msstore' }               # The latest ver stable with pyTorch (imgGen AI), as of 4/13/2024
                @{ Name = 'DevToys'; Source = 'msstore' }                   # JSON formatting, text comparing, testing RegExp, Base64 encoder/decoder...
                @{ Name = 'SlackTechnologies.Slack'; Source = 'winget' }    # Like Discord, but for business
                @{ Name = 'sharex'; Source = 'winget' }                     # Screenshot + GIF util with arrows/infographic tools + easy uploader to imgur
                @{ Name = 'oh-my-posh'; Source = 'msstore' }                # Prettify your terminal # TODO: Move to a new dev app configure func?
            )

            Install-AppsViaWinget -WingetApps $WingetApps
        }
        
        # Add an `elevate` command to PowerShell to run the terminal as admin in the current working directory
        function Setup-ElevateCmd
        {
            # Check if the PowerShell profile exists
            if (-not (Test-Path -Path $PROFILE)) {
                # Create the profile if it does not exist
                New-Item -ItemType File -Path $PROFILE -Force
            }

            # Load the current content of the profile
            $currentProfileContent = Get-Content -Path $PROFILE

            # Check if the 'Elevate' function is already defined
            if (-not ($currentProfileContent -match 'function Elevate')) {
                # If not, add a newline and the function to the end of the profile
                Add-Content -Path $PROFILE -Value "`nfunction Elevate {`n    Start-Process wt -ArgumentList '-d', (Get-Location) -Verb RunAs`n}`n"
                # Display a success message indicating the addition
                Write-Host "Successfully added `elevate` command to $PROFILE"
            }
            else
            {
                # Display a message indicating the command already exists
                Write-Host "`elevate` command already exists in $PROFILE"
            }
        }

        Install-ChocoDevApps
        Install-WingetDevApps
        dotnet workload install wasi-experimental # Allows WASM-WASI devving in .NET 8+
        RefreshTerminal

        Setup-PrettyTerminal # Requires choco `oh-my-posh` (installed above)
        Setup-ElevateCmd # Type `elevate` in powershell to bump to admin in same working dir
        Install-ForkGitClient
    }

    # nvm: Node.js version manager for Windows
    # Installs and sets `lts` ver
    function Install-NvmNodeNpmYarn
    {
        function setup-NvmNodeNpm
        {
            $env:NVM_HOME="C:\ProgramData\nvm" # Workaround ^
            $env:NVM_SYMLINK="C:\Program Files\nodejs" # Workaround ^
            nvm install lts
            RefreshTerminal
        }

        echo ""
        echo "[Install-DevTools] Installing node + npm + yarn through nvm >>"
        setup-NvmNodeNpm

        nvm install lts
        nvm use lts
    }

    # We won't overwrite existing keys
    function Init-SshKeys
    {
        $pathToSshKey = "$env:USERPROFILE\.ssh\id_ed25519"

        # Check if the SSH key file already exists
        if (Test-Path $pathToSshKey) {
            Write-Output ""
            Write-Output "[Init-SshKeys] SSH key already exists at '$pathToSshKey'. No new key generated."
            return
        }

        Write-Output ""
        Write-Output "[Install-DevTools] Initializing SSH keys (ed25519) ..."

        Write-Output ""
        Write-Output "Creating SSH key to '$pathToSshKey'"
        mkdir $env:USERPROFILE/.ssh -Force # -Force ensures the directory is created without throwing an error if it already exists
        ssh-keygen -t ed25519 -f $pathToSshKey -C $WorkEmail -N '""'

        # TODOs and SSH agent setup
        Write-Output "[Install-DevTools] Setting ssh-agent to 'Manual' startup-type (+starting) ..."
        Get-Service ssh-agent | Set-Service -StartupType Manual
        Start-Service ssh-agent # Ensure ssh-agent is started

        # Additional commented out TODOs remain for further implementation
    }

    echo ""
    echo "[Install-DevTools] Installing dev apps >>"
    Install-VS-Tools
    Install-CoreDevTools
    Install-NvmNodeNpmYarn
    Init-SshKeys
}

function Install-HomeApps
{
    function Install-CoreHomeAppsViaChoco
    {
        echo ""
        echo "[Install-CoreHomeApps] Installing core home apps (7z, Slack, Python, etc)  ..."

        $ChocoHomeApps = @(
            'hwinfo' # Sort of like CPU-Z and GPU-Z combined; new PC? Double check your specs!
			'expressvpn'
            'googlechrome --ignore-checksums'
            'firefox'
			'brave' # My personal favorite [Chromium-based] browser used in place of Chrome
            #'tor-browser' # Talk smack on Reddit without big brother watching # Now included in Brave browser
            'discord' # Superior chat app
            'foxitreader' # PDF
            'vlc' # Video viewer better than Windows default; plays most file types
            'k-litecodecpackfull' # Codecs to help play a variety of video types
            'teamviewer' # Remote control your other PCs or friends that know nothing of IT
            'googledrive'
            'th-ch-youtube-music' # YouTube Music, but open source version without the memory leaks
            #'authy-desktop' # (!) Best not to use this unless you're stuck: Desktop app goes away + they hold your keys hostage w/no export opts! I personally switche to 2fas.com
            'transmission' # Torrent client (for legal use only) # Prevent spam: Apply blocklist from: https://github.com/Naunter/BT_BlockLists/raw/master/bt_blocklists.gz | src: https://gist.github.com/shmup/29566c5268569069c256
            #'peerblock' # Block spammy network peers # TODO: Broken atm, but follow-up later.
            'powertoys' # Official Microsoft power user utils: Eg, rename a giant batch of images, file locksmith
            #'lockhunter --ignore-checksums' # Find/unlock/kill proc locking files/dirs # Now included in powertoys as "locksmith"
            'treesizefree' # Find what's eating all your space on your drive with ease
            'partitionwizard' # View or config drive partitions
            'steam --ignore-checksums' # Game platform client
	        #'vmware-workstation-player' # BUG: Conflicts with "Hyper-V" # Potentially resolved with HypervisorPlatform API! TODO: Test.
            'epicgameslauncher' # Also for Unreal Engine installs
            'eraser' # Secure GUI file shredder
            'sdelete' # secure CLI file shredder
            'audacity' # Open src audio editor; nice for gamedev to simple crop/edit of sound tracks or sfx
            'shutup10' # Customize win11, remove telemetry at-will, etc
        )

        # Nvidia?
        if ( Assert-IsNvidiaGpu )
        {
            $ChocoHomeApps += 'geforce-experience' # Nvidia only
        }
        else
        {
            # !NVIDIA: Perhaps AMD, Intel, etc
            echo ""
            echo "[Install-CoreHomeApps] Skipping Nvidia GeForce Experience app ..."
            # Manually install "AMD Software" @ https://www.amd.com/en/support/kb/faq/rsx2-install
        }

        Install-AppsViaChoco -ChocoApps $ChocoHomeApps
        RefreshTerminal
    }

    function Install-NicheHomeApps
    {
        function Install-CoreHomeAppsViaChoco
        {
            $ChocoHomeApps = @(
                'google-voice-desktop'  # Free Google VoIP
                'streamlabs-obs'        # SLOBS for Twitch streaming and screencasting
                'plexmediaserver'       # Stream your movies/shows/vids on your TV
                'imageglass'            # ImageGlass 9: Modern, open src image viewer (with webp support); replace default "Photos" | https://apps.microsoft.com/detail/9n33vzk3c7th?hl=en-US&gl=US
            )

            Install-AppsViaChoco -ChocoApps $ChocoHomeApps
        }

        function Install-CoreHomeAppsViaWinget
        {
            $WingetApps = @(
                @{ Name = "WhatsApp"; Source = "msstore" }              # Useful for "Western" countries
                @{ Name = "LINE Desktop"; Source = "msstore" }          # Useful for folks in eastern Asia
                @{ Name = "OBS Studio"; Source = "msstore" }            # Video recording and streaming
                @{ Name = "Adobe Creative Cloud"; Source = "msstore" }  # Photoshop, etc
                @{ Name = "Revo Uninstaller"; Source = "winget" }       # Uninstall annoying apps like a champ
            )

            Install-AppsViaWinget -WingetApps $WingetApps
        }

        echo ""
        echo "[Install-CoreHomeApps] Installing niche home apps (Plex, Line, etc)  ..."

        Install-CoreHomeAppsViaChoco
        RefreshTerminal
    }

    echo ""
    echo "[Install-HomeApps] Installing home apps >>"
    Install-CoreHomeAppsViaChoco
    Install-NicheHomeApps
}
#endregion /Installs


# ----------------------------------------------------------------------
#region WinConfig

# Debloat + Privacy/Telemetry disable >>
# https://github.com/TheWorldOfPC/Windows11-OptimizePrivacy-Guide
function Optimize-WindowsApps
{
    param(
        [bool]$IsPrivateNetwork,
        [string]$PreferredDns,
        [string]$PreferredDnsAlt
    )

    # Some may be preinstalled; some not. See entire list via ps:
    # `Get-AppxPackage -AllUsers | Select Name, PackageFullName`
    function Rm-BloatyApps
    {
        echo ""
        echo "[Optimize-WindowsApps] Removing preinstalled apps that no one wants (OK to ignore errors, here) ..."

        $BloatyAppxPkgs = @(
            "Facebook.InstagramBeta"
            "AdobeSystemsIncorporated.AdobeCreativeCloudExpress"
            "AmazonVideo.PrimeVideo"
            "BytedancePte.Ltd.TikTok"
            "9E2F88E3.Twitter"
            "king.com.CandyCrushSodaSaga"
            "5A894077.McAfeeSecurity"
            "Microsoft.MixedReality.Portal"
            "Microsoft.Office.Sway" # Office Trial
            "Microsoft.Office.Desktop" # Office Trial
            "Microsoft.SkypeApp"
            "*OneNote*" # Office Trial
        )

        # Remove each app, if exists. After uninstalled, verify it was uninstalled (else show err, but continue)
        foreach ($BloatyAppxPkg in $BloatyAppxPkgs) {
            $apps = Get-AppxPackage -Name $BloatyAppxPkg -AllUsers

            if ($apps.Count -gt 0) {
                foreach ($app in $apps) {
                    try {
                        if ($app -ne $null) {
                            Write-Output ""
                            Write-Output "[Optimize-WindowsApps] Found $($app.Name) - Removing (via AppxPackage) ..."
                            Remove-AppxPackage -Package $app.PackageFullName

                            # Verify if the application was actually removed
                            $appRemoved = Get-AppxPackage -Name $BloatyAppxPkg -AllUsers | Where-Object { $_.PackageFullName -eq $app.PackageFullName }
                            if ($appRemoved -eq $null) {
                                Write-Output "[Optimize-WindowsApps] $($app.Name) uninstall success."
                            } else {
                                Write-Error "[Optimize-WindowsApps] Error: $($app.Name) uninstall !success."
                            }
                        }
                    } catch {
                        Write-Error "An error occurred while removing $($app.Name): $_"
                    }
                }
            } else {
                # Write-Output "[Optimize-WindowsApps] No package found with name $BloatyAppxPkg"
            }
        }

        echo ""
        echo "[Optimize-WindowsApps] Removing Disney+ ..."
        try {
            winget uninstall "Disney+" --silent --accept-source-agreements
            Write-Output "[Optimize-WindowsApps] Disney+ has been uninstalled."
        } catch {
            Write-Error "Failed to uninstall Disney+: $_"
        }

        Rm-MoreCortana
    }

#    # Disable Wi-Fi Sense -- connects you automatically to "safe" hotspots
#    function Disable-WiFiSense
#    {
#        echo ""
#        echo "[Optimize-WindowsApps] Disabling Wi-Fi Sense ..."
#        If (!(Test-Path "HKLM:\Software\Microsoft\PolicyManager\default\WiFi\AllowWiFiHotSpotReporting")) {
#            New-Item -Path "HKLM:\Software\Microsoft\PolicyManager\default\WiFi\AllowWiFiHotSpotReporting" -Force | Out-Null
#        }
#
#        echo ""
#        echo "[Optimize-WindowsApps] Disabling Wi-Fi HotSpot Reporting ..."
#        Set-ItemProperty -Path "HKLM:\Software\Microsoft\PolicyManager\default\WiFi\AllowWiFiHotSpotReporting" -Name "Value" -Type DWord -Value 0
#
#        echo ""
#        echo "[Optimize-WindowsApps] Disabling Wi-Fi Sense HotSpot Auto-Reconnecting ..."
#        Set-ItemProperty -Path "HKLM:\Software\Microsoft\PolicyManager\default\WiFi\AllowAutoConnectToWiFiSenseHotspots" -Name "Value" -Type DWord -Value 0
#
#        # Enable Wi-Fi Sense
#        # Set-ItemProperty -Path "HKLM:\Software\Microsoft\PolicyManager\default\WiFi\AllowWiFiHotSpotReporting" -Name "Value" -Type DWord -Value 1
#        # Set-ItemProperty -Path "HKLM:\Software\Microsoft\PolicyManager\default\WiFi\AllowAutoConnectToWiFiSenseHotspots" -Name "Value" -Type DWord -Value 1
#    }

#    function Disable-AdvertisingId
#    {
#        echo ""
#        echo "[Optimize-WindowsApps] Disabling Advertising ID (via Registry) ..."
#
#        If (!(Test-Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\AdvertisingInfo")) {
#            New-Item -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\AdvertisingInfo" | Out-Null
#        }
#
#        Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\AdvertisingInfo" -Name "Enabled" -Type DWord -Value 0
#    }

#    function Disable-TaskTelemetry
#    {
#        echo ""
#        echo "[Optimize-WindowsApps] Disabling Task Telemetry..."
#
#        # Task Telemetry: https://gist.github.com/thoroc/86d354d029dda303598a
#        schtasks /Change /TN "Microsoft\Windows\Application Experience\ProgramDataUpdater" /Disable | out-null
#        #schtasks /Change /TN "Microsoft\Windows\AppID\SmartScreenSpecific" /Disable | out-null
#        schtasks /Change /TN "Microsoft\Windows\Application Experience\Microsoft Compatibility Appraiser" /Disable | out-null
#        schtasks /Change /TN "Microsoft\Windows\Customer Experience Improvement Program\Consolidator" /Disable | out-null
#        #schtasks /Change /TN "Microsoft\Windows\Customer Experience Improvement Program\KernelCeipTask" /Disable | out-null
#        schtasks /Change /TN "Microsoft\Windows\Customer Experience Improvement Program\UsbCeip" /Disable | out-null
#        schtasks /Change /TN "Microsoft\Windows\DiskDiagnostic\Microsoft-Windows-DiskDiagnosticDataCollector" /Disable | out-null
#        schtasks /Change /TN "Microsoft\Windows\NetTrace\GatherNetworkInfo" /Disable | out-null
#        schtasks /Change /TN "Microsoft\Windows\Windows Error Reporting\QueueReporting" /Disable | out-null
#        # Not sure about the following task, but the reg hack doesn't work either, so this is a pain in the fucking ass, maybe someone will figure it out, leaving it here:
#        # schtasks /Change /TN "Microsoft\Windows\SettingSync\BackgroundUploadTask" /Disable | Out-Null
#        # TakeOwnership-RegKey "LocalMachine" "SOFTWARE\Microsoft\Windows NT\CurrentVersion\Schedule\TaskCache\Tasks" | Out-Null
#        # New-Item -ErrorAction SilentlyContinue -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Schedule\TaskCache\Tasks\{00524425-019B-4FDD-B1C5-04767424D01B}" -Force | Out-Null
#        # New-ItemProperty -ErrorAction SilentlyContinue -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Schedule\TaskCache\Tasks\{00524425-019B-4FDD-B1C5-04767424D01B}" -Name "Triggers" -PropertyType Binary -Value ([byte[]](0x17,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0x00,0xff,0xff,0xff,0xff,0xff,0xff,0xff,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x4a,0x85,0x00,0x42,0x48,0x48,0x48,0x48,0xd9,0x2b,0x30,0x29,0x48,0x48,0x48,0x48,0x0c,0x00,0x00,0x00,0x48,0x48,0x48,0x48,0x55,0x00,0x73,0x00,0x65,0x00,0x72,0x00,0x73,0x00,0x00,0x00,0x48,0x48,0x48,0x48,0x00,0x00,0x00,0x00,0x48,0x48,0x48,0x48,0x00,0x48,0x48,0x48,0x48,0x48,0x48,0x48,0x00,0x48,0x48,0x48,0x48,0x48,0x48,0x48,0x05,0x00,0x00,0x00,0x48,0x48,0x48,0x48,0x0c,0x00,0x00,0x00,0x48,0x48,0x48,0x48,0x01,0x01,0x00,0x00,0x00,0x00,0x00,0x05,0x04,0x00,0x00,0x00,0x48,0x48,0x48,0x48,0x00,0x00,0x00,0x00,0x48,0x48,0x48,0x48,0x58,0x00,0x00,0x00,0x48,0x48,0x48,0x48,0x00,0x00,0x00,0x00,0x30,0x2a,0x00,0x00,0x80,0xf4,0x03,0x00,0xff,0xff,0xff,0xff,0x07,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xa2,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x02,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00)) -Force | Out-Null
#        cmd /c sc config diagnosticshub.standardcollector.service start= disabled | out-null # Service
#        cmd /c sc config TrkWks start= disabled | out-null # Service
#        Set-Content C:\ProgramData\Microsoft\Diagnosis\ETLLogs\AutoLogger\AutoLogger-Diagtrack-Listener.etl -Value "" -Force # Empties DiagTrack log
#
#        # More task telemetry: https://www.powershellgallery.com/packages/AppVeyorBYOC/1.0.100-beta/Content/scripts%5CWindows%5Cdisable_scheduled_tasks.ps1
#        Disable-ScheduledTask -TaskPath '\Microsoft\Windows\DiskDiagnostic' -TaskName 'Microsoft-Windows-DiskDiagnosticResolver'
#        Disable-ScheduledTask -TaskPath '\Microsoft\Windows\DiskDiagnostic' -TaskName 'Microsoft-Windows-DiskDiagnosticDataCollector'
#        Disable-ScheduledTask -TaskPath '\Microsoft\Windows\DiskFootprint' -TaskName 'Diagnostics'
#        Disable-ScheduledTask -TaskPath '\Microsoft\Windows\Diagnosis' -TaskName Scheduled
#        Disable-ScheduledTask -TaskPath '\Microsoft\Windows\Customer Experience Improvement Program' -TaskName Consolidator
#        Disable-ScheduledTask -TaskPath '\Microsoft\Windows\Customer Experience Improvement Program' -TaskName UsbCeip
#        Disable-ScheduledTask -TaskPath '\Microsoft\Windows\Power Efficiency Diagnostics' -TaskName 'AnalyzeSystem'
#        Disable-ScheduledTask -TaskPath '\Microsoft\Windows\Windows Error Reporting' -TaskName 'QueueReporting'
#
#        # Unnecessary tasks: https://www.powershellgallery.com/packages/AppVeyorBYOC/1.0.100-beta/Content/scripts%5CWindows%5Cdisable_scheduled_tasks.ps1
#        Disable-ScheduledTask -TaskPath '\Microsoft\Windows\Defrag' -TaskName ScheduledDefrag
#        Disable-ScheduledTask -TaskPath '\Microsoft\Windows\Chkdsk' -TaskName ProactiveScan
#        Disable-ScheduledTask -TaskPath '\Microsoft\Windows\Autochk' -TaskName Proxy
#        Disable-ScheduledTask -TaskPath '\Microsoft\Windows\Maps' -TaskName 'MapsToastTask'
#        Disable-ScheduledTask -TaskPath '\Microsoft\Windows\Windows Defender' -TaskName 'Windows Defender Scheduled Scan' # This always happens at the worst moment
#    }

#    function Disable-AppAccessToWinData
#    {
#        echo ""
#        echo "[Optimize-WindowsApps] Disabling app access to WinData (like camera, langauge, etc) ..."
#
#        echo ""
#        echo "[Optimize-WindowsApps] Disabling Privacy -> General -> Let websites provide locally relevant content by accessing language list ..."
#        if ((Get-ItemProperty -Path "HKCU:SOFTWARE\Microsoft\Internet Explorer\International\" -Name AcceptLanguage -ErrorAction SilentlyContinue) -ne $null) { Remove-ItemProperty -Path "HKCU:SOFTWARE\Microsoft\Internet Explorer\International" -Name "AcceptLanguage" -Force }
#        Set-ItemProperty -ErrorAction SilentlyContinue -Path "HKCU:Control Panel\International\User Profile" -Name HttpAcceptLanguageOptOut -Value 1 | Out-Null
#
#        echo ""
#        echo "[Optimize-WindowsApps] Disabling Privacy -> General -> turn on smartscreen filter to check web content that windows store apps use ..."
#        Set-ItemProperty -ErrorAction SilentlyContinue -Path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\AppHost\" -Name EnableWebContentEvaluation -Value 0 -Force | Out-Null
#
#        echo ""
#        echo "[Optimize-WindowsApps] Disabling Privacy -> Camera -> let apps use my camera ..."
#        #Set-ItemProperty -ErrorAction SilentlyContinue -Path "HKCU:SOFTWARE\Microsoft\Windows\CurrentVersion\DeviceAccess\Global\{E5323777-F976-4f5b-9B55-B94699C46E44}" -Name Value -Value "Deny" | Out-Null
#
#        echo ""
#        echo "[Optimize-WindowsApps] Disabling Privacy -> Microphone -> let apps use my microphone ..."
#        #Set-ItemProperty -ErrorAction SilentlyContinue -Path "HKCU:SOFTWARE\Microsoft\Windows\CurrentVersion\DeviceAccess\Global\{2EEF81BE-33FA-4800-9670-1CD474972C3F}\" -Name Value -Value "Deny" | Out-Null
#
#        echo ""
#        echo "[Optimize-WindowsApps] Disabling Privacy -> Account info -> let apps access my name, picture and other account info ..."
#        Set-ItemProperty -ErrorAction SilentlyContinue -Path "HKCU:SOFTWARE\Microsoft\Windows\CurrentVersion\DeviceAccess\Global\{C1D23ACC-752B-43E5-8448-8D0E519CD6D6}\" -Name Value -Value "Deny" | Out-Null
#
#        echo ""
#        echo "[Optimize-WindowsApps] Disabling Privacy -> Calendar -> let apps access my calendar ..."
#        Set-ItemProperty -ErrorAction SilentlyContinue -Path "HKCU:SOFTWARE\Microsoft\Windows\CurrentVersion\DeviceAccess\Global\{D89823BA-7180-4B81-B50C-7E471E6121A3}\" -Name Value -Value "Deny" | Out-Null
#
#        echo ""
#        echo "[Optimize-WindowsApps] Disabling Privacy -> Messaging -> let apps read or send sms and text messages ..."
#        Set-ItemProperty -ErrorAction SilentlyContinue -Path "HKCU:SOFTWARE\Microsoft\Windows\CurrentVersion\DeviceAccess\Global\{992AFA70-6F47-4148-B3E9-3003349C1548}\" -Name Value -Value "Deny" | Out-Null
#
#        echo ""
#        echo "[Optimize-WindowsApps] Disabling Privacy -> Radio -> let apps control radios ..."
#        Set-ItemProperty -ErrorAction SilentlyContinue -Path "HKCU:SOFTWARE\Microsoft\Windows\CurrentVersion\DeviceAccess\Global\{A8804298-2D5F-42E3-9531-9C8C39EB29CE}\" -Name Value -Value "Deny" | Out-Null
#
#        ## Privacy -> Other devices -> sync with devices
#        #Set-ItemProperty -ErrorAction SilentlyContinue -Path "HKCU:SOFTWARE\Microsoft\Windows\CurrentVersion\DeviceAccess\Global\LooselyCoupled\" -Name Value -Value "Deny" | Out-Null
#
#        echo ""
#        echo "[Optimize-WindowsApps] Privacy -> Feedback & Diagnostics -> feedback frequency ..."
#        New-Item -ErrorAction SilentlyContinue -Path "HKCU:SOFTWARE\Microsoft\Siuf\Rules" -Force | Out-Null
#        Set-ItemProperty -ErrorAction SilentlyContinue -Path "HKCU:SOFTWARE\Microsoft\Siuf\Rules" -Name NumberOfSIUFInPeriod -Value 0 -Force | Out-Null
#        if ((Get-ItemProperty -Path "HKCU:SOFTWARE\Microsoft\Siuf\Rules" -Name PeriodInNanoSeconds -ErrorAction SilentlyContinue) -ne $null) { Remove-ItemProperty -Path "HKCU:SOFTWARE\Microsoft\Siuf\Rules" -Name PeriodInNanoSeconds }
#
#        ## Ease of Access -> Other options -> Visual options -> play animations
#        #Set-ItemProperty -ErrorAction SilentlyContinue -Path "HKCU:Control Panel\Desktop\WindowMetrics" -Name MinAnimate -Value 0 | Out-Null
#    }

#    function Append-HostFileEntriesToBlockTelemetry
#    {
#        echo ""
#        echo "[Optimize-WindowsApps] Appending host file entries to block telemetry ..."
#
#        $HostFile = "C:\Windows\System32\drivers\etc\hosts"
#        $Localhost = "127.0.0.1"
#        $TelemetrySite = @(
#            'vortex.data.microsoft.com'
#            'vortex-win.data.microsoft.com'
#            'telecommand.telemetry.microsoft.com'
#            'telecommand.telemetry.microsoft.com.nsatc.net'
#            'oca.telemetry.microsoft.com'
#            'oca.telemetry.microsoft.com.nsatc.net'
#            'sqm.telemetry.microsoft.com'
#            'sqm.telemetry.microsoft.com.nsatc.net'
#            'watson.telemetry.microsoft.com'
#            'watson.telemetry.microsoft.com.nsatc.net'
#            'redir.metaservices.microsoft.com'
#            'choice.microsoft.com'
#            'choice.microsoft.com.nsatc.net'
#            'df.telemetry.microsoft.com'
#            'reports.wes.df.telemetry.microsoft.com'
#            'services.wes.df.telemetry.microsoft.com'
#            'sqm.df.telemetry.microsoft.com'
#            'telemetry.microsoft.com'
#            'watson.ppe.telemetry.microsoft.com'
#            'telemetry.appex.bing.net'
#            'telemetry.urs.microsoft.com'
#            'telemetry.appex.bing.net:443'
#            'vortex-sandbox.data.microsoft.com'
#            'settings-sandbox.data.microsoft.com'
#            'vortex.data.microsoft.com'
#            'vortex-win.data.microsoft.com'
#            'telecommand.telemetry.microsoft.com'
#            'telecommand.telemetry.microsoft.com.nsatc.net'
#            'oca.telemetry.microsoft.com'
#            'oca.telemetry.microsoft.com.nsatc.net'
#            'sqm.telemetry.microsoft.com'
#            'sqm.telemetry.microsoft.com.nsatc.net'
#            'watson.telemetry.microsoft.com'
#            'watson.telemetry.microsoft.com.nsatc.net'
#            'redir.metaservices.microsoft.com'
#            'choice.microsoft.com'
#            'choice.microsoft.com.nsatc.net'
#            'vortex-sandbox.data.microsoft.com'
#            'settings-sandbox.data.microsoft.com'
#            'df.telemetry.microsoft.com'
#            'reports.wes.df.telemetry.microsoft.com'
#            'sqm.df.telemetry.microsoft.com'
#            'telemetry.microsoft.com'
#            'watson.microsoft.com'
#            'watson.ppe.telemetry.microsoft.com'
#            'wes.df.telemetry.microsoft.com'
#            'telemetry.appex.bing.net'
#            'telemetry.urs.microsoft.com'
#            'survey.watson.microsoft.com'
#            'watson.live.com'
#            'services.wes.df.telemetry.microsoft.com'
#            'telemetry.appex.bing.net'
#            'vortex.data.microsoft.com'
#            'vortex-win.data.microsoft.com'
#            'telecommand.telemetry.microsoft.com'
#            'telecommand.telemetry.microsoft.com.nsatc.net'
#            'oca.telemetry.microsoft.com'
#            'oca.telemetry.microsoft.com.nsatc.net'
#            'sqm.telemetry.microsoft.com'
#            'sqm.telemetry.microsoft.com.nsatc.net'
#            'watson.telemetry.microsoft.com'
#            'watson.telemetry.microsoft.com.nsatc.net'
#            'redir.metaservices.microsoft.com'
#            'choice.microsoft.com'
#            'choice.microsoft.com.nsatc.net'
#            'df.telemetry.microsoft.com'
#            'reports.wes.df.telemetry.microsoft.com'
#            'wes.df.telemetry.microsoft.com'
#            'services.wes.df.telemetry.microsoft.com'
#            'sqm.df.telemetry.microsoft.com'
#            'telemetry.microsoft.com'
#            'watson.ppe.telemetry.microsoft.com'
#            'telemetry.appex.bing.net'
#            'telemetry.urs.microsoft.com'
#            'telemetry.appex.bing.net:443'
#            'settings-sandbox.data.microsoft.com'
#            'vortex-sandbox.data.microsoft.com'
#            'survey.watson.microsoft.com'
#            'watson.live.com'
#            'watson.microsoft.com'
#            'statsfe2.ws.microsoft.com'
#            'corpext.msitadfs.glbdns2.microsoft.com'
#            'compatexchange.cloudapp.net'
#            'cs1.wpc.v0cdn.net'
#            'a-0001.a-msedge.net'
#            'a-0002.a-msedge.net'
#            'a-0003.a-msedge.net'
#            'a-0004.a-msedge.net'
#            'a-0005.a-msedge.net'
#            'a-0006.a-msedge.net'
#            'a-0007.a-msedge.net'
#            'a-0008.a-msedge.net'
#            'a-0009.a-msedge.net'
#            'msedge.net'
#            'a-msedge.net'
#            'statsfe2.update.microsoft.com.akadns.net'
#            'sls.update.microsoft.com.akadns.net'
#            'fe2.update.microsoft.com.akadns.net'
#            'diagnostics.support.microsoft.com'
#            'corp.sts.microsoft.com'
#            'statsfe1.ws.microsoft.com'
#            'pre.footprintpredict.com'
#            'i1.services.social.microsoft.com'
#            'i1.services.social.microsoft.com.nsatc.net'
#            'feedback.windows.com'
#            'feedback.microsoft-hohm.com'
#            'feedback.search.microsoft.com'
#        )
#
#        # Example: "127.0.0.1 vortex.data.microsoft.com" | Out-File -encoding ASCII -append $File
#        # For each telemetrySite, add a line to the hosts file
#        echo ""
#        foreach ($TelemetrySite in $TelemetrySites) {
#            echo "[Optimize-WindowsApps] Appending $TelemetrySite [spammy/telemtrics site] to network Host file (blacklist) ..."
#            "$Localhost $TelemetrySite" | Out-File -encoding ASCII -append $File
#        }
#    }

    function Set-NetworkPrivate
    {
        echo ""
        echo "[Optimize-WindowsApps] Setting network to Private ..."

        # Set network to Private
        Get-NetConnectionProfile -Name "Network" -NetworkCategory Private
    }

    # Sets DOH for CloudFlare >> Sets primary/secondary DNS
    function Set-PreferredDns
    {
        # https://learn.microsoft.com/en-us/powershell/module/dnsclient/set-dnsclientserveraddress?view=windowsserver2022-ps
        # Set DNS to preferred (default CloudFlare)
        echo ""
        echo "[Optimize-WindowsApps] Setting preferred DNS to $PreferredDns / $PreferredDnsAlt ..."
        echo "[Optimize-WindowsApps] For Ethernet..."
        Set-DnsClientServerAddress "Ethernet" -ServerAddresses {$PreferredDns,$PreferredDnsAlt}

        echo "[Optimize-WindowsApps] For Wi-Fi..."
        Set-DnsClientServerAddress "Wi-Fi" -ServerAddresses {$PreferredDns,$PreferredDnsAlt}

        ipconfig /flushdns
    }

    echo ""
    echo "[Optimize-WindowsApps] Removing bloaty apps ..."
    #Rm-BloatyApps
    #Prevent-AppsFromReinstalling
    #Disable-WiFiSense
    #Disable-AdvertisingId
    #Disable-TaskTelemetry
    #Disable-AppAccessToWinData
    #Append-HostFileEntriesToBlockTelemetry
    Set-PreferredDns

    if ($IsPrivateNetwork)
    {
        Set-NetworkPrivate
    }
}

function Set-WinDevSettings
{
    param(
        [string]$DisableShowMoreOptsRightClick
    )

#    # Script by ThisIsWin11 @ https://github.com/builtbybel/ThisIsWin11
#    # Certain elements of Windows 11, such as the News and interests widget, open links in Edge regardless of which
#    # browser is set as default. This is done through the use of edge:// protocol links.
#    # This script intercepts URIs that force-open web links in Microsoft Edge and redirects it to the system's default web browser
#    # To revert to Edge as the default handler for web searches, all you have to do is run the script again.
#    # Projects page: https://github.com/AveYo/fox/blob/main/ChrEdgeFkOff.cmd
#    function Redirect-LinksToDefaultBrowser
#    {
#        echo ""
#        echo "[Set-WinDevSettings] Redirecting hard-coded Bing/Edge links to default browser ..."
#
#        @(set "0=%~f0"^)#) & powershell -nop -c iex([io.file]::ReadAllText($env:0)) & exit/b
#        #:: double-click to run or just copy-paste into powershell - it's a standalone hybrid script
#        #::
#        #:: ChrEdgeFkOff - make start menu web search or widgets links open in your chosen default browser - by AveYo
#        #:: v2.0 only redirects microsoft-edge: links, no longer blocks msedge.exe (with a junction trick)
#        #::
#        $_Paste_in_Powershell = {
#            $vbs = @'
#' ChrEdgeFkOff - make start menu web search or widgets links open in your chosen default browser - by AveYo
#Dim C, A: For Each i in WScript.Arguments: A = A&" """&i&"""": Next
#Set W = CreateObject("WScript.Shell"): Set E = W.Environment( "Process" ): E("CL") = A : C = ""
#C = C & "$U = get-itemproperty 'HKCU:\SOFTWARE\Microsoft\Windows\Shell\Associations\UrlAssociations\https\UserChoice' 'ProgID';"
#C = C & "$C = get-itemproperty -lit $('Registry::HKCR\' + $U.ProgID + '\shell\open\command') '(Default)' -ea 0;"
#C = C & "$UserChoice = ($C.'(Default)'-split [char]34,3)[1]; $MSE = $env:CL -replace '\\Microsoft\\Edge', '\Microsoft\ChrEdge';"
#C = C & "if ($UserChoice -like '*Microsoft\Edge\Application\msedge.exe*') {iex('&'+$MSE); return 2};"
#C = C & "if ($env:CL -notlike '*microsoft-edge:*') {iex('&'+$MSE); return 1};"
#C = C & "start $UserChoice $([uri]::unescapedatastring(($env:CL-split'(?=http[s]?)')[1] -replace [char]34)+' '); return 0"
#W.Run "powershell -nop -c " & C, 0, False
#'@
#            $DATA = [Environment]::GetFolderPath('CommonApplicationData'); $file = join-path $DATA "ChrEdgeFkOff.vbs"
#            $PROF = [Environment]::GetFolderPath('ProgramFiles'+('x86','')[![Environment]::Is64BitOperatingSystem])
#            $EDGE = join-path $PROF 'Microsoft\Edge\'; $CREDGE = join-path $PROF 'Microsoft\ChrEdge\'
#            $IFEO = 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Image File Execution Options\msedge.exe'
#            if (test-path "$IFEO\0") {
#                remove-item "$IFEO\0" -rec -force -ea 0 >''; remove-itemproperty $IFEO 'Debugger' -force -ea 0 >''
#                del $file -force -ea 0 >''; rmdir $CREDGE -rec -force -ea 0 >'';
#                write-host -fore 0xf -back 0xd "`n ChrEdgeFkOff v2.0 [REMOVED] run again to install "
#            } else {
#                new-item "$IFEO\0" -force -ea 0 >''; remove-itemproperty $IFEO 'Debugger' -force -ea 0 >''
#                [io.file]::WriteAllText($file, $vbs) >''; start -nonew cmd "/d/x/r mklink /J ""$CREDGE"" ""$EDGE"" >nul"
#                set-itemproperty $IFEO 'UseFilter' 1 -type dword -force -ea 0
#                set-itemproperty "$IFEO\0" 'FilterFullPath' $(join-path $PROF 'Microsoft\Edge\Application\msedge.exe') -force -ea 0
#                set-itemproperty "$IFEO\0" 'Debugger' "wscript $file //B //T:5" -force -ea 0
#                write-host -fore 0xf -back 0x2 "`n ChrEdgeFkOff v2.0 [INSTALLED] run again to remove " } ; timeout /t 5
#        } ; start -verb runas powershell -args "-nop -c & {`n`n$($_Paste_in_Powershell-replace'"','\"')}"
#        $_Press_Enter
#        #::
#
#
#        ## Scripts by ThisIsWin11 @ https://github.com/builtbybel/ThisIsWin11 (modified by Dylan) | Projects page: https://github.com/rcmaehl/MSEdgeRedirect###
#        ## This will download and run a third-party utility with a GUI whichs allows you to redirect News, Search, Widgets, Weather and more to your default Browser.###
#        #$downloadsDir = "$env:userprofile\Downloads"
#        #$msEdgeRedirectPathToExe = "$downloadsDir/MSEdgeRedirect.exe"
#        #(New-Object System.Net.WebClient).DownloadFile('https://github.com/rcmaehl/MSEdgeRedirect/releases/latest/download/MSEdgeRedirect.exe', $msEdgeRedirectPathToExe)
#        #Start-Process -Filepath $msEdgeRedirectPathToExe # OPENS DIALOGUE, but does it async (script will continue).
#        #
#        ## Script by ThisIsWin11 @ https://github.com/builtbybel/ThisIsWin11 | Projects page: https://github.com/AveYo/fox/blob/main/ChrEdgeFkOff.cmd
#        ## Certain elements of Windows 11, such as the News and interests widget, open links in Edge regardless of which browser is set as default. This is done through the use of edge:// protocol links. ###
#        ## This script intercepts URIs that force-open web links in Microsoft Edge and redirects it to the system's default web browser ###
#        ## To revert to Edge as the default handler for web searches, all you have to do is run the script again. ###
#        #& '.\modules\Edge\Bypass Edge as default handler for web searches.ps1'
#    }

    # From Decrapfier @ https://github.com/n1snt/Windows-Decrapifier
    # Hide Search button / box
    function Hide-SearchBoxBtn
    {
        echo ""
        echo "[Set-WinDevSettings] Hiding search box / button ..."
        Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Search" -Name "SearchboxTaskbarMode" -Type DWord -Value 0
    }

    ## Optional TODO
    # function Add-SecondaryChinesePinYinTraditionalKeyboard
    # {
    #     #Write-Host "Adding secondary Chinese Pinyin Traditional keyboard..."
    #     #$langs = Get-WinUserLanguageList
    #     #$langs.Add("en-US")
    #     #Set-WinUserLanguageList $langs -Force
    # }

    function Show-HiddenFilesAndExtensions
    {
        echo ""
        echo "[Set-WinDevSettings] Showing hidden files and extensions ..."

        # Set some reg keys for customizations below
        Push-Location
        Set-Location HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced
        Set-ItemProperty . HideFileExt "0" # Show file extensions
        Set-ItemProperty . Hidden "1" # Show hidden files
        Pop-Location
    }

    function Enable-DarkTheme
    {
        echo ""
        echo "[Set-WinDevSettings] Enabling system-wide dark theme ..."
        Set-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize -Name AppsUseLightTheme -Value 0
    }

    function Allow-LongPaths
    {
        echo ""
        echo "[Set-WinDevSettings] Allowing long paths ..."

        # Allow long paths
        New-ItemProperty `
            -Path "HKLM:\SYSTEM\CurrentControlSet\Control\FileSystem" `
            -Name "LongPathsEnabled" `
            -Value 1 `
            -PropertyType DWORD `
            -Force
    }

    # Enable "ultimate power plan" mode (great for desktop; terrible for laptops).
    # This only ENABLES it so you can see it; you need to MANUALLY select this.
    # Found via https://computersluggish.com/windows11/optimisation-maintenance/windows-11-add-ultimate-performance-power-plan/
    function Unlock-UltimatePowerPlan
    {
        echo ""
        echo "[Set-WinDevSettings] Unlocking ultimate power plan ..."
        powercfg -duplicatescheme e9a42b02-d5df-448d-aa00-03f14749eb61
    }

    function Set-ExplorerToOpenToThisPC
    {
        echo ""
        echo "[Set-WinDevSettings] Setting explorer to open to This PC ..."

        New-ItemProperty -ErrorAction SilentlyContinue `
            -Path "HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Advanced" `
            -Name LaunchTo `
            -PropertyType DWORD `
            -Value 1 `
            -Force `
            | Out-Null
    }

    function Remove-CustomizeThisFolderFromContextMenu
    {
        echo ""
        echo "[Set-WinDevSettings] Removing 'Customize this folder' from context menu ..."

        New-Item -ErrorAction SilentlyContinue `
            -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" `
            -Force `
            | Out-Null

        New-ItemProperty -ErrorAction SilentlyContinue `
            -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Policies\Explorer" `
            -Name NoCustomizeThisFolder `
            -Value 1 `
            -PropertyType DWORD `
            -Force `
            | Out-Null
    }

    # Remove 'Share with' from context menu (First 9 might be superflous, just in case)
    function Remove-SharedWithFromContextMenu
    {
        echo ""
        echo "[Set-WinDevSettings] Removing 'Shared with' from context menu ..."

        Remove-Item -ErrorAction SilentlyContinue -Path "HKCR:\Directory\Background\shellex\ContextMenuHandlers\Sharing" -Force -Recurse | Out-Null
        Remove-Item -ErrorAction SilentlyContinue -Path "HKCR:\Directory\shellex\ContextMenuHandlers\Sharing" -Force -Recurse | Out-Null
        reg delete "HKEY_CLASSES_ROOT\*\shellex\ContextMenuHandlers\Sharing" /f | Out-Null
        Remove-Item -ErrorAction SilentlyContinue -Path "HKCR:\Directory\shellex\CopyHookHandlers\Sharing" -Force -Recurse | Out-Null
        Remove-Item -ErrorAction SilentlyContinue -Path "HKCR:\Directory\shellex\PropertySheetHandlers\Sharing" -Force -Recurse | Out-Null
        Remove-Item -ErrorAction SilentlyContinue -Path "HKCR:\Drive\shellex\ContextMenuHandlers\Sharing" -Force -Recurse | Out-Null
        Remove-Item -ErrorAction SilentlyContinue -Path "HKCR:\Drive\shellex\PropertySheetHandlers\Sharing" -Force -Recurse | Out-Null
        Remove-Item -ErrorAction SilentlyContinue -Path "HKCR:\LibraryFolder\background\shellex\ContextMenuHandlers\Sharing" -Force -Recurse | Out-Null
        Remove-Item -ErrorAction SilentlyContinue -Path "HKCR:\UserLibraryFolder\shellex\ContextMenuHandlers\Sharing" -Force -Recurse | Out-Null
        New-ItemProperty -ErrorAction SilentlyContinue -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced" `
            -Name SharingWizardOn -PropertyType DWORD -Value 0 -Force | Out-Null
    }

    # Remove 'Include in library' from context menu (might be superflous, just in case)
    function Rm-IncludeInLibFromContextMenu
    {
        echo ""
        echo "[Set-WinDevSettings] Removing 'Include in library' from context menu ..."

        Remove-Item -ErrorAction SilentlyContinue "HKCR:\Folder\ShellEx\ContextMenuHandlers\Library Location" -Force -Recurse | Out-Null
        Remove-Item -ErrorAction SilentlyContinue "HKLM:\SOFTWARE\Classes\Folder\ShellEx\ContextMenuHandlers\Library Location" -Force -Recurse | Out-Null
    }

    function Remove-ObsoleteFeats
    {
        echo ""
        echo "[Set-WinDevSettings] Removing obsolete features (XPS, Work Folders) ..."

        # XPS Services
        Dism /online /Disable-Feature /FeatureName:Printing-XPSServices-Features /quiet /norestart

        # Work Folders
        Dism /online /Disable-Feature /FeatureName:WorkFolders-Client /quiet /norestart

        # Enable.NET 3.5 framework because some programs still use it
        #Dism /online /Enable-Feature /FeatureName:NetFx3 /quiet /norestart
    }

    # Enable Windows Development Mode (then Reboot)
    function Enable-WinDevMode
    {
        echo ""
        echo "[Set-WinDevSettings] Enabling Windows Development Mode (active after reboot) ..."

        reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\AppModelUnlock" `
            /t REG_DWORD /f /v "AllowDevelopmentWithoutDevLicense" /d "1"
    }

#    # Disable Bing-based web suggestions in Start menu search:
#    # Found via https://isc.sans.edu/diary/Making+Windows+10+a+bit+less+%22Creepy%22++-+Common+Privacy+Settings/21947
#    function Disable-StartMenuSuggestions
#    {
#        echo ""
#        echo "[Set-WinDevSettings] Disabling start menu suggestions ..."
#
#        Set-ItemProperty -Path "HKCU:\Software\Microsoft\Windows\CurrentVersion\Search" `
#            -Name "BingSearchEnabled" `
#            -Type DWord `
#            -Value 0
#
#        # Disable search box suggestions: https://4sysops.com/archives/turn-off-web-search-in-windows-11-using-group-policy/
#        if ( -not (Test-Path -Path HKLM:\SOFTWARE\Policies\Microsoft\Windows\Explorer))
#        {
#            New-Item HKLM:\SOFTWARE\Policies\Microsoft\Windows\Explorer
#        }
#
#        Set-ItemProperty -Path HKLM:\SOFTWARE\Policies\Microsoft\Windows\Explorer `
#            -Name "DisableSearchBoxSuggestions" -Value 1 -Type DWord
#    }

    function Install-DevFonts
    {
        echo ""
        echo "[Set-WinDevSettings] Installing Cascadia Code PL dev font ..."
        Install-AppsViaChoco -ChocoApps "cascadiacodepl"
        RefreshTerminal
    }

    # Almost all of these require reboot later
    # (!) If you install VMWare, enable this first!
    function Enable-WinProFeatsIfPro
    {
        echo ""
        echo "[Set-WinDevSettings] Checking if Win version == Pro ..."

        $isProEdition = [bool](Get-WindowsEdition -online | Where-Object Edition -eq "Professional")
        if ( $isProEdition )
        {
            echo ""
            echo "[Set-WinDevSettings] Found Win Pro! Enabling a few Win Pro optional features (requires reboot later) ..."

            # Hyper-V | https://www.makeuseof.com/how-to-add-remove-optional-features-windows-11/
            echo ""
            echo "[Set-WinDevSettings] Enabling Hyper-V (virtualization) ..."
            Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All -NoRestart

            echo ""
            echo "[Set-WinDevSettings] Enabling HypervisorPlatform API (prevents conflicts with VMWare 16+ and maybe VBox) ..."
            Enable-WindowsOptionalFeature -Online -FeatureName HypervisorPlatform -All -NoRestart

            # Windows Sandbox (insta-Windows-VM) | https://learn.microsoft.com/en-us/windows/security/threat-protection/windows-sandbox/windows-sandbox-overview
            echo ""
            echo "[Set-WinDevSettings] Enabling WinSandbox (insta-VM) ..."
	    $feature = Get-WindowsOptionalFeature -Online -FeatureName "Containers-DisposableClientVM"
	    if (-not $feature.State) {
	        Enable-WindowsOptionalFeature -Online -FeatureName "Containers-DisposableClientVM" -All -NoRestart
	    } else {
	        Write-Host "Feature is already enabled. Skipping..."
	    }
        }
    }

    # src: "How To Disable “Show More Options” In Windows 11 Context Menu And Restore Old Menu" @
    # https://www.itechtics.com/disable-show-more-options-in-windows-11-context-menu-and-restore-old-menu/?unapproved=364475&moderation-hash=304a51e6f719e295ac37744aa45ea36a#comment-364475
    # (!) This may or may not cause issues in 2024
    function Disable-ShowMoreOptsContextMenu
    {
        reg add "HKCU\Software\Classes\CLSID\{86ca1aa0-34aa-4e8b-a509-50c905bae2a2}\InprocServer32" /ve /d "" /f
    }

    echo ""
    echo "[Set-WinDevSettings] Configuring Windows dev settings >>"

    #Redirect-LinksToDefaultBrowser # Edge is getting better, lately
    Hide-SearchBoxBtn
    #Add-SecondaryChinesePinYinTraditionalKeyboard # Uncomment if you want to add a traditional PinYin traditional keyboard
    Show-HiddenFilesAndExtensions
    Enable-DarkTheme # Save your retinas
    Allow-LongPaths # If you have a long/path/nested/like/this.txt, it can crash things without this tweak
    Force-WinExplorerRestart # Refresh the changes without reboot
    Unlock-UltimatePowerPlan # Useful for desktops, or when laptops are plugged in
    #Set-ExplorerToOpenToThisPC # The default open location is !useful
    Remove-CustomizeThisFolderFromContextMenu # Bloaty context menu item
    Remove-SharedWithFromContextMenu # Bloaty context menu item
    Remove-ObsoleteFeats # Fax, etc
    Enable-WinDevMode # Dev mode is nice - Google it
    #Disable-StartMenuSuggestions # Start menu bloat
    Install-DevFonts
    Enable-WinProFeatsIfPro # Such as 'Sandbox' (a 5-second spun up Win11 VM; amazing)

    if ($DisableShowMoreOptsRightClick)
    {
        Disable-ShowMoreOptsContextMenu
    }
}
#endregion /WinConfig