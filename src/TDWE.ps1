################################################################################################################
# PERSONAL REMINDERS LIST (TODO: Wipe this for public)
# Don't forget to open Nvidia Experience and update your gfx driver!
# SLOBS for video streaming
# Disable "Delivery Optimization" via "Windows Update > Advanced Opts > Delivery"
# Disable privacy telemetrics via UI (safer than Registry) and/or use ShutupWindows
# Grab apps:
# - Veeam backup | https://www.veeam.com/windows-backup-free-download.html 
# - Open JetBrains toolbox and download your JB apps

################################################################################################################
$ver = "v1"
$ConfigPathToYaml = ".\config.yaml"
$PathToManifest = ".\private\TheDevsWinEnv-Manifest.psd1";
$PathToNextScript = ".\private\TDWE-AfterPwsh7.ps1"
################################################################################################################

# Relative path to "TheDevsWinEnv-Manifest.psd1"
function Import-AllModules
{
    echo ""
    echo "[TDWE] Importing $ver manifest of modules (via psd1 manifest) ..."
    ipmo -Name $PathToManifest -Force
}

# -----------------------------------------------------------------------------------
# Init >>
$pathToPs1Dir = ([IO.FileInfo] $MyInvocation.MyCommand.Path).Directory.Parent.FullName + "\src"

echo ""
echo "[TDWE] Changing working dir to script path @ '$pathToPs1Dir' ..."
cd $pathToPs1Dir

Import-AllModules
Assert-RunAsAdmin
Import-Config -ConfigPathToYaml $ConfigPathToYaml
echo ""
echo "[TDWE] Silently Installing - this will take a while >>"

# -----------------------------------------------------------------------------------   

# Main >>
Backup-CoreRegistry
if ($PauseBetweenSections) { Write-Host "Finished Backup-CoreRegistry (Next: InstallWsl2Ubuntu). " -NoNewline; pause }

Install-Wsl2Ubuntu `
    -Wsl2LinuxDistro $Wsl2LinuxDistro # Install before everything else! Fails if pending reboot.
if ($PauseBetweenSections) { Write-Host "Finished Install-Wsl2Ubuntu (Next: Install-CorePkgMgrs). " -NoNewline; pause }

Install-CorePkgMgrs
if ($PauseBetweenSections) { Write-Host "Finished Install-CorePkgMgrs (Next: Install-Git). " -NoNewline; pause }

Install-Git `
    -GitRootDir $GitRootDir `
    -FullName $FullName `
    -$WorkEmail WorkEmail
if ($PauseBetweenSections) { Write-Host "Finished Install-Git (Next: Install-Pwsh). " -NoNewline; pause }

Install-Pwsh7
if ($PauseBetweenSections) { Write-Host "Finished Install-CorePkgMgrs (Next: Set-WinDevSettings, launched in a NEW Window in the upgraded PowerShell 7 'pwsh'. This window will exit. " -NoNewline; pause }

Start-Process -FilePath "pwsh" -ArgumentList "-NoExit", "-File", "$PathToNextScript"
Exit
